package test;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Application;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.mysql.cj.api.jdbc.Statement;

@ApplicationPath("/")
@Path("hello")
@Produces(MediaType.APPLICATION_JSON)
public class Test extends Application {
	@GET
	public Response getHello(@QueryParam("param") String example) {
		//This is just to show you that the JDBC dependency in maven is working
		Statement statement;
		return Response.ok().entity("Hello World! " + example).build();
	}
	
	@GET
	@Path("{id}")
	public Response getByID(@PathParam("id") String id ){
		return Response.ok().entity("is is " + id).build();
	}
	
	@GET
	@Path("goodbye")
	public Response getGoodbye(@QueryParam("temp") String message) {
		if (message != null)
			return Response.ok().entity("See ya! " + message).build();
		else
			return Response.ok().entity("See ya!").build();
		
	}
}